import java.util.Scanner;
public class Jackpot{
	public static void main(String[]args){
		
		System.out.println("Hello there! Are you ready to participate in this Jackpot???");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		int wins = 0;
		boolean playAgain = true;
		
		//loop to rpeat if user wants to replay
		while (playAgain)
		{   //loop that repeats while gameOver is not true
			while(!gameOver)
			{
				System.out.println(board);
				if(board.playATurn())
				{
					gameOver = true;
				}
				else
				{
					numOfTilesClosed ++;
				}
			}
			
			if(numOfTilesClosed >= 7)
			{
				System.out.println("We have a winner!!! You wan the big Jackpot of 1000$!");
				wins ++;
			}
			else 
			{
				System.out.println("Sadly, you have lost...better luck next time!");
			}
			
			System.out.print("Would you like to play again? (y/n) ");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.next();
			if (input.equalsIgnoreCase("n")) 
			{
				playAgain = false;
			}
			gameOver = false;
			numOfTilesClosed = 0;
			System.out.println("You won " + wins + " times.");
		}
	}
}
		