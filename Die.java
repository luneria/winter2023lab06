import java.util.Random;
public class Die
{
	private int faceValue;
	private Random random;
	
	//constructor
	public Die()
	{
		this.faceValue = 1;
		this.random = new Random();
	}
	
	//getter
	public int getFaceValue()
	{
		return this.faceValue;
	}
	
	//instance method
	public void roll()
	{
		this.faceValue = random.nextInt(1,6);
	}
	
	//toString
	public String toString()
	{
		return Integer.toString(this.faceValue);
	}	
}